const express = require("express");
const connectDb = require("./config/database");
const errorHandler = require("./middleware/errorHandler");
const dotenv = require("dotenv").config();
const mongoose = require("mongoose");
const dbConfig = require("./config/database");

const app = express();


mongoose.connect(dbConfig.url).then(() => {
  console.log("Db connected to " + process.env.DB_HOST + process.env.DB_NAME);
}).catch(err => {
  console.log(err);
  process.exit();
});

const port = process.env.PORT || 3000;

app.use(express.json());
app.use("/contacts", require("./routes/contactRoutes"));
app.use("/users", require("./routes/userRoutes"));
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});